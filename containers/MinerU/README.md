# MienrU

MinerU es una aplicacion de conversion de documentos, con la ayuda de varios modelos 
de lenguaje, vision, reconocimiento, difusion, entre otros, puede convertir 
unaa gran variedad de archivos a otro, como y no limitado a:

* PDF 
* DOCX 
* MD 
* PPT 

Que son los mas populares, los desarrolladores son chinos y manejan el proyecto con licencia
GPL de codigo abierto asi que cualquiera puede usar el software y codigo.

# Instalacion mediante Contenedores y con uso de GPU NVIDIA

## **Primero designar un area en donde se generara la imagen y se descargaran los archivos necesarios:**

```powershell
PS1> Invoke-WebRequest -Uri 'https://github.com/opendatalab/MinerU/raw/master/docker/global/Dockerfile' -OutFile Dockerfile
```

## Construir la imagen con docker:

```
PS1> docker build -t mineru:latest .
```
<br>

<br>

###### _Nota:_
> Sino estas seguro de que tu maquina pueda aprovechar o no haz configurado
> bien que esta este disponible como recurso para docker ejecuta 
> el siguiente comando:
> ```powershell
> PS1> docker run --rm --gpus=all nvidia/cuda:12.1.0-base-ubuntu22.04 nvidia-smi
> ```

Si el resulultado/OutPut de ese comando te meustra a pantalla el resumen de 
`nvidia-smi` su tarjeta esta bien configurada de lo contrario no prosigas e 
investiga otro metodo de instalacion.

<br>

## Activar el entorno interno del contenedor:

```powershell
PS1> docker run --rm -it --gpus=all mineru:latest /bin/bash -c "echo 'source /opt/mineru_venv/bin/activate' >> ~/.bashrc && exec bash"
```

# Confirmar la correcta ejecucion de la aplicacion:

```powershell
PS1> magic-pdf --help
```

Si todo salio bien deberiamos de poder visualizar el menu de ayuda de la aplicacion.
