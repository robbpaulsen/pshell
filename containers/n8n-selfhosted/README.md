# `n8n` (pronunciado _EN-EIGT-EN_)

Te ayuda a conectar cualquier aplicación con una API directamente con otra y
manipular sus datos con poco o nada de código.

* Personalizable: flujos de trabajo altamente flexibles y la opción de crear
nodos personalizados.
* Conveniente: usa npm o Docker para probar n8n.
* Centrado en la privacidad: aloja n8n por tu cuenta para mayor privacidad y seguridad.

Aqui yace el Compose File mas simple para iniciar en modo self hosting de `n8n`

1. Inicia la compilacion del servicio con:

```docker
docker compose up -d .
```

> _Esto se hace en el directorio raiz en donde se encuntra el `docker-compose.yml` file_

2. Genera una cuenta gratuita y sigue los prompts para la subscripcion.
3. Inicia tu primer flujo de automatizacion.

##### **Proyecto raiz de n8n:**
<https://github.com/n8n-io/n8n.git>
