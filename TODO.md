# [22/SEP/2024]
___

<br>

- [ ] *En el flujo del instalador cuando se estan ubicando los archivos de configuracion de la terminal de windows, se envian 3 Logos a 'C:\temp' y el `json` de configuracion se envia a la ruta de %LOCALAPPDATA% pero no estan tomando efecto los atajos de teclado y no se muestran los logos tampoco de Ubuntu, Kali y PowerShell. hay que revisar en que version del proyecto se borro de mas algun parametro.*

- [ ] *Si ya no esta presente la funcion de completado estilo IDE en el pwsh profile, tal vez sea mejor omitir la instalcion por completo del paquete con scoop ya que ningun otro vendor lo tiene aun, al igual que `vincent` terminal, ya tenemos 300+ temas de colores para la terminal.*

- [ ] *Hay que incluir las variables de entorno que configuramos todo este tiempo, y es muy tedioso. Tambien hay que respaldarlas.*

- [x] *Y de paso respaldar la maquina =)*


# *Para futura referencia:
Por alguna razon los drives configurados en software RAID evitan
de alguna manera el display post boot y los discos empiean a lanzar 
sus codigo de error de sonido. Por si se repite el evento hay que etiquetqar
los satas con el numero de socket al que van conectados y solo dejar el drive
que contiene el arranque y/o sistema operativo funcionando y conectado/s
unavez que arranque por primera vez y se reconfigure la BIOS y guardar los cambios
para reiniciar la maquina, en el reinicio hay que sincronizar el conectar los cables
SATA de los drives en RAID justo despues de que se aprecie el prearranque para los discos
no se quieran inicializar o indexar y que la BIOS busque arranque en ellos, SI SE CONECTAN YA CUANDO
EL LOGO DE LA TARJETA MADRE ESTE VISBLE HAY QUE REINICAR Y VOLVER A INTENTAR, solo asi
no seran leidos por la busqueda de arranque y esto es lo que congela el boot,
ya en grub solo esperar a entrar en LIVE solo se debe de montar el pool RAID y generar
un INODE, ya con el INODE suncronizar la escritura con `sudo sync`, desmontar el RAID y
reiniciar. Ya asi el RAID vovleria a la normalidad.

<br>

____

26/oct/2024

<br>

- [ ] *En el flujo del instalador cuando se estan ubicando los archivos de configuracion de la terminal de windows, se envian 3 Logos a 'C:\temp' y el `json` de configuracion se envia a la ruta de %LOCALAPPDATA% pero no estan tomando efecto los atajos de teclado y no se muestran los logos tampoco de Ubuntu, Kali y PowerShell. hay que revisar en que version del proyecto se borro de mas algun parametro.*
- [x] ~~*Si ya no esta presente la funcion de completado estilo IDE en el pwsh profile, tal vez sea mejor omitir la instalcion por completo del paquete con scoop ya que ningun otro vendor lo tiene aun, al igual que `vincent` terminal, ya tenemos 300+ temas de colores para la terminal.*~~
- [x] ~~*Hay que incluir las variables de entorno que configuramos todo este tiempo, y es muy tedioso. Tambien hay que respaldarlas.*~~
- [x] ~~*Y de paso respaldar la maquina =)*~~


# *Para futura referencia:
[x] ~~Por alguna razon los drives configurados en software RAID evitan
de alguna manera el display post boot y los discos empiean a lanzar 
sus codigo de error de sonido. Por si se repite el evento hay que etiquetqar
los satas con el numero de socket al que van conectados y solo dejar el drive
que contiene el arranque y/o sistema operativo funcionando y conectado/s
unavez que arranque por primera vez y se reconfigure la BIOS y guardar los cambios
para reiniciar la maquina, en el reinicio hay que sincronizar el conectar los cables
SATA de los drives en RAID justo despues de que se aprecie el prearranque para los discos
no se quieran inicializar o indexar y que la BIOS busque arranque en ellos, SI SE CONECTAN YA CUANDO
EL LOGO DE LA TARJETA MADRE ESTE VISBLE HAY QUE REINICAR Y VOLVER A INTENTAR, solo asi
no seran leidos por la busqueda de arranque y esto es lo que congela el boot,
ya en grub solo esperar a entrar en LIVE solo se debe de montar el pool RAID y generar
un INODE, ya con el INODE suncronizar la escritura con `sudo sync`, desmontar el RAID y
reiniciar. Ya asi el RAID vovleria a la normalidad.~~

<br>

____
